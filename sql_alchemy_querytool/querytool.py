from new_model import *
import datetime
"""
This hopefully will be the main querytool for Aiida after the transition to SQLAlchemy is done
"""
# NOTES verdi calculation list eventually calls _list_calculations in aiida.orm.calculation.job.__init__
# First query to get PKs

# transitive closure
# data can only have one input!

replacement_dict = {
    float:'float', 
    int:'int',
    JSONB:'jsonb',
    str:'str'
    }
replacement_dict.update({val.class_:key for key, val in Node.__mapper__.polymorphic_map.items()})
replacement_dict.update({val:val for val in replacement_dict.values()})

ormclass_dict = Node.__mapper__.polymorphic_map
reverse_ormclass_dict = {val:key for key, val in ormclass_dict.items()}

def flatten(l):
    #~ print len(l)
    if type(l) not in (list, set, tuple):
        return [l]
    return_list = []
    [[return_list.append(i) for i in flatten(item)] for item in l]
    return return_list
    

def make_json_compatible(inp):
    """
    A little function to make a queryhelp json - compatible.
    This means that it can be stored as a node in the database and
    retrieved or shared.
    All classes defined in the input are converted to strings specifying the type,
    for example:
    
    -   ``float`` --> "float"
    -   ``Struc`` --> "node.data.struc"
    """
    if isinstance(inp, dict):
        for key, val in inp.items():
            inp[make_json_compatible(key)] = make_json_compatible(inp.pop(key))
    elif type(inp) in (list, tuple):
        inp = [make_json_compatible(val) for val in inp]
    else:
        try:
            inp = replacement_dict.get(inp, inp)
        except Exception as e:
            raise Exception("""
            Exception thrown: {}\n
            while replacing {}""".format(e, inp))
    return inp


def get_expr(operator, value, db_path, val_in_json):
    """
    Applies a filter on the alias given.
    Expects the alias of the ORM-class on which to filter, and filter_spec.
    Filter_spec contains the specification on the filter.
    Expects:
    
    -   ``operator``: The operator to apply. These can be:
    
        -  for any type: 
            -   ==  (compare single value, eg: '==':5.0)
            - in    (compare whether in list, eg: 'in':[5, 6, 34]
        -  for floats and integers:
             - >
             - <
             - <=
             - >= 
        -  for strings:
             - like  (case - sensitive)   (eg 'like':'node.calc.%'  will match node.calc.relax and node.calc.RELAX and node.calc. but node node.CALC.relax)
             - ilike (case - unsensitive) (will also match node.CaLc.relax)
                
            on that topic:
            The character % is a reserved special character in SQL, and acts as a wildcard.
            If you specifically want to capture a ``%`` in the string name, use:
            ``_%``
        -  for arrays and dictionaries:
             - contains  (pass a list with all the items that the array should contain, or that should be among the keys, eg: 'contains': ['N', 'H'])
             - has_key   (pass an element that the list has to contain or that has to be a key, eg: 'has_key':'N')
        -  for arrays only:
             - of_length  
             - longer
             - shorter
        
        All the above filters invoke a negation of the expression if preceded by ~
        
        - {'name':{'~in':['halle', 'lujah']}} # Name not 'halle' or 'lujah'
        - {'id':{ '~==': 2}} # id is not 2

        
    - ``value``: The value for the right side of the expression, the value you want to compare with.
    - ``db_bath``: The path leading to the value
    - ``val_in_json``: Boolean, whether the value is in a json-column, requires type casting.


        
    TODO:
    
    -   implement redundant expressions for user-friendliness: 
    
        -   ~==: !=, not, ne 
        -   ~in: nin, !in
        -   ~like: unlike    
    """
    def cast_according_to_type(path_in_json, value, val_in_json):
        if not val_in_json:
            return path_in_json
        
        elif isinstance(value, int):
            return path_in_json.cast(Integer)
        
        elif isinstance(value, float):
            return path_in_json.cast(Float)
        
        elif isinstance(value, list):
            return path_in_json.cast(JSONB)
        
        elif isinstance(value, tuple):
            return path_in_json.cast(JSONB)
        
        elif isinstance(value, dict):
            return path_in_json.cast(JSONB)
        
        elif isinstance(value, str):
            return path_in_json.astext
            
        elif isinstance(value, datetime.datetime):
            return path_in_json.cast(TIMESTAMP)
        else:
            raise Exception( ' Unknown type {}'.format(type(value)))
        
    
    if operator.startswith('~'):
        negation = True
        operator = operator.lstrip('~')
    else:
        negation = False
    if operator == '==':
        expr = cast_according_to_type(db_path, value, val_in_json) == value
    elif operator == '>':
        expr = cast_according_to_type(db_path, value, val_in_json) > value 
    elif operator == '<':
        expr = cast_according_to_type(db_path, value, val_in_json) < value 
    elif operator == '>=':
        expr = cast_according_to_type(db_path, value, val_in_json) >= value 
    elif operator == '<=':
        expr = cast_according_to_type(db_path, value, val_in_json) <= value 
    elif operator == 'like':
        if val_in_json:
            expr = db_path.astext.like(value)
        else:
            expr = db_path.like(value)
    
    elif operator == 'ilike':
        if val_in_json:
            expr = db_path.astext.ilike(value)
        else:
            expr = db_path.ilike(value)
    
    elif operator == 'in':
        value_type_set = set([type(i) for i in value])
        if len(value_type_set) > 1:
            raise Exception( '{}  contains more than one type'.format(value))
                
        casted_column = cast_according_to_type(db_path, value[0], val_in_json)
        expr = casted_column.in_(value)
    elif operator == 'contains':
        #~ print 'I must contain', value
        # This only works for json
        expr = db_path.cast(JSONB).contains(value)
    elif operator == 'has_key':
        #~ print 'I must contain', value
        expr = db_path.cast(JSONB).has_key(value)
    elif operator == 'of_length':
        expr = jsonb_array_length(db_path.cast(JSONB)) == value
    elif operator == 'longer':
        expr = jsonb_array_length(db_path.cast(JSONB)) > value
    elif operator == 'shorter':
        expr = jsonb_array_length(db_path.cast(JSONB)) < value
    elif operator == 'nr_of_keys':
        #~ print 'I must contain', value
        expr = jsonb_dict_length(db_path.cast(JSONB)) == value
    elif operator == 'and':
        and_expressions_for_this_path = []
        for filter_operation_dict in value:
            for newoperator, newvalue in filter_operation_dict.items():
                and_expressions_for_this_path.append(get_expr(newoperator, newvalue, db_path, val_in_json))
        expr = and_(*and_expressions_for_this_path)
    elif operator == 'or':
        or_expressions_for_this_path = []
        for filter_operation_dict in value:
            # Attention: Multiple expression inside one direction are joint by and!
            # Default will and should always be kept AND
            or_expressions_for_this_path.append(and_(*[get_expr(newoperator, newvalue, db_path, val_in_json)
                for newoperator, newvalue in filter_operation_dict.items()]))
            #~ for newoperator, newvalue in filter_operation_dict.items():
                #~ or_expressions_for_this_path.append(get_expr(newoperator, newvalue, db_path, val_in_json))
        expr = or_(*or_expressions_for_this_path)
    else:
        raise Exception (' Unknown filter {}'.format(operator))
    
    if negation:
        return not_(expr)
    else:
        return expr


class Querytool():
    """
    
    This is a query-builder for SQLAlchemy. 
    The specifications are as follows:
    
      1) The querytool can join the tables `Node` and `Link` an arbitrary number of times, automatizing the aliasing of the tables.
      2) Supports basic filter operations on regular columns ( =, like, in, >, <, >=, <=)
      3) Supports the negations of the filters specified in 2) 
      4) Supports a few filter operations in the json:
        - dictionaries: has_key for the keys, 
        - in arrays: contains, of_length
        - in values inside the json: all expressions defined in 2) and 3).
      5) Supports projection of columns
      6) Supports projection of JSONElements.
      7) Allows the query-specification to be stored in a JSON.
         The query could be treated as a calculation, with an input (the queryhelp)
        
    TODO:
        1) Transitive closure
        2) filter on type of link, label of link
    
    """

    ######## EVERYTHING TO HELP BUILD THE API (QUERYHELP) IS HERE!

    def __init__(self,  queryhelp, caller_cls = None):
        """
        I expect the queryhelp here. 
        The queryhelp is my API.
        It is a list that tells me:
        
            -  what to join, 
            -  what to project and
            -  how to filter.
        
        The queryhelp is a dictionary. 
        Currently, 3 keys can be given: "path", "projection", "filters"
        The contain information about the path to be joined along, the items to be projected, and the filters to be applied.
        Only the path is a necessary key.
                
        *   Specifying the path:
            The user specifies here the path along which to join tables.
            The value is a list, each listitem being a vertice in your path.
            You can define the vertice in two ways:
            The first is to give the orm-class::
          
                queryhelp = {
                    'path':[
                        {'class':Data}
                    ]
                }
                # Data is a subclass of Node
            
            The second is to give the polymorphic identity of this class, in our case stored in type::

                queryhelp = {
                    'path':[
                        {'class':"node.data"}
                    ]
                }
            
            
            Each node has to have a unique label.
            By default the polymorphic identity is chosen, but if that might not be unique if the 
            user chooses the same class twice.
            In this case he has to provide a label::
            
                queryhelp = {
                    'path':[
                        {
                            'class':"node.data",
                            'label':'data_2'
                        }
                    ]
                }
            
            
            There also hast to be some information on the edges, in order to join correctly.
            There are several redundant ways this can be done:
            
            -   You can specify that this node is an input or output of another node 
                preceding the current one in the list.
                That other node can be specified by an integer or the class or type.
                The following examples are all valid joining instructions, 
                assuming there is a structure defined at index 2 of the path with label "struc1"::
                    
                    edge_specification = queryhelp['path'][3]
                    edge_specification['output_of'] = 2
                    edge_specification['output_of'] = Struc
                    edge_specification['output_of'] = 'struc1'
                    edge_specification['input_of']  = 2
                    edge_specification['input_of']  = Struc
                    edge_specification['input_of']  = 'struc1'
                
                .. note:: If you specify the labels, make sure they are unique! 
          
            -   queryhelp_item['direction'] = integer
                
                If any of the above specs ("input_of", "output_of") 
                were not specified, the key "direction" is looked for.
                Directions are defined as distances in the tree. 
                1 is defined as one step down the tree along a link.
                This means that 1 joins the node specified in this dictionary
                to the node specified on list-item before **as an output**.
                Direction defaults to 1, which is why, if nothing is specified,
                this node is joined to the previous one as an output by default.
                A minus sign reverse the direction of the link.
                The absolute value of the direction defines the table to join to
                with respect to your own position in the list.
                An absolute value of 1 joins one table above, a
                value of 2 to the table defined 2 indices above.
                The two following queryhelps yield the same  query::
                  
                    qh2 = {
                        'path':[
                            {'class':MD}, 
                            {'class':Traj}, 
                            {
                                'class':Param,  
                                'direction':-2
                            }
                        ]
                    }
                    
                    # returns same query as:
                    
                    qh3 = {
                        'path':[
                            {'class':MD}, 
                            {'class':Traj}, 
                            {'class':Param,  'input_of':MD}
                        ]
                    }
                    
                
                An MD-calculation can produce a trajectory and some parameters, and both queryhelps above
                would return the same result.
        
        *   Projecting: 
            Determing which columns the query will return.
            
            Let's start with an easy example::
            
                queryhelp = {
                    'path':[{'class':Relax}],
                    'project':{
                        Relax:['state', 'id'],
                    }
                }
                
                # OR
                
                queryhelp = {
                    'path':[{'class':Relax}],
                    'project':{
                        'node.calc.relax':['state', 'id'],
                    }
                }
                
                    
            You can also project a value stored inside the json::
            
                queryhelp = {
                    'path':[
                        {'class':Relax},
                        {'class':Struc}
                        
                        ],
                    'project':{
                        'node.calc.relax':['state', 'id'],
                        Struc:['attributes.cell']
                    }
                }
          
            Returns the state and the id of all instances of Relax and 
            the cells of all structures given that the structures are linked as output of a relax-calculation.
            The strings that you pass have to be name of the columns.
            If you pass a star ('*') or ``True``, the query will return the instance of the ormclass.
            

        *   Filtering:
            What if I want not every structure, 
            but only the ones that were added after a certain time `t` and have an id higher than 50::
                
                queryhelp = {
                    'path':[{'class':Relax}, {'class':Struc}],
                    'filters':{
                        Struc:[
                            {
                                'time':{'>': t},
                                'id':{'>': 50}
                            }
                        ]
                    }
                }
                
            With the key 'filters', we instruct the querytool to build filters and attach them to the query.
            Filters are passed as dictionaries.
            In each key, value, the key is presents the column-name (as a string) to filter on.
            The value is another dictionary, where the operator is a key and the value is the 
            value to check against.
            
            .. note:: This follows the MongoDB-syntax and is necessary to deal with "and" and "or"-clauses
            
            But what if the user wants to filter by key-value pairs defined inside the structure?
            In that case, simply specify the path with the dot (`.`) being a separator.
            If you want to get to the volume of the structure, stored in the attributes, you can specify::
                
                queryhelp = {
                    'path':[{'class':Struc}],
                    'filters':{
                        'attributes.volume': {'<':6.0}
                    }
                }
            
            The above queryhelp would build a query that returns all structures with a volume below 6.0.
            
            .. note:: A big advantage of SQLAlchemy is that it support the storing of jsons. 
                      It is convenient to dump the structure-data into a json and store that as a column.
                      The querytool needs to be told how to query the json.
                      
        
        Let's get to a really complex use-case,
        where we need to reconstruct a workflow:
            1) The MD-simulation with the parameters and structure that were used as input
            2) The trajectory that was returned as an output
            3) We are only interested in calculations that had a convergence threshold
               smaller than 1e-5 and cutoff larger 60 (quantities stored in the parameters)
            4) In the parameters, we only want to load the temperature
            5) The MD simulation has to be in state "parsing" or "finished"
            6) We want the length of the trajectory
            7) We filter for structures that:
            
                - Have any lattice vector smaller than 3.0 or between 5.0 and 7.0
                - Contain Nitrogen and Hydrogen
                - Do not contain S
                - Have 4 atoms
                - Have less than 3 types of atoms (elements)
                
        This would be the queryhelp::
            
            queryhelp =  {
                'path':[
                    {'class':Param}, 
                    {
                        'class':MD,
                        'label':'md'
                    },
                    {'class':Traj}, 
                    {
                        'class':Struc, 
                        'input_of':'md'
                    },
                    {
                        'class':Relax,
                        'input_of':Struc
                    },
                    {
                        'class':Struc,
                        'label':'struc2',
                        'input_of':Relax
                    }
                ],
                'project':{
                    Param:'attributes.IONS.tempw',
                    'md':['id', 'time'], 
                    Traj:[
                        'id', 
                        'attributes.length'
                    ],  
                    Struc:[
                        'id',
                        'name', 
                        'attributes.atoms',
                        'attributes.cell'
                    ], 
                    'struc2':[
                        'id',
                        'name', 
                        'attributes.atoms',
                        'attributes.cell'
                    ],
                },
                'filters':{
                    Param:{   
                        'and':[
                            {'attributes.SYSTEM.econv':{'<':1e-5}},
                            {'attributes.SYSTEM.ecut':{'>':60}}
                        ]
                            
                    },
                    'md':[
                        {
                            'state':{   
                                'in':(
                                    'computing', 
                                    'parsing', 
                                    'finished',
                                    'new'
                                )
                            }
                        }
                    ],
                    Struc:[
                        {
                            'or':[
                                {
                                    'attributes.cell.0.0':{
                                        'or':[
                                            {'<':3.0},
                                            {'>':5., '<':7.}
                                        ]
                                    },
                                },
                                {
                                    'attributes.cell.1.1':{
                                        'or':[
                                            {'<':3.0},
                                            {'>':5., '<':7.}
                                        ]
                                    },
                                },
                                {
                                    'attributes.cell.2.2':{
                                        'or':[
                                            {'<':3.0},
                                            {'>':5., '<':7.}
                                        ]
                                    },
                                },
                            ]
                            
                        },
                        {
                            'attributes.atoms':{
                                'contains':['N', 'H'],
                                '~has_key':'S'
                            }
                        },
                        {
                            'attributes.atoms':{
                                'of_length':4
                            }
                        },
                        {
                            'attributes.elements':{
                                'shorter':3,
                                'has_key':'N',
                            }
                        }
                    ],
                    
                }
            }
            
            
        """
            
        # Make it json-compatible:
        self.path = []
        self.alias_dict = {}
        queryhelp = copy.deepcopy(queryhelp)


        self.projection_dict_user = make_json_compatible(queryhelp.pop('project', {}))
        self.filters = make_json_compatible(queryhelp.pop('filters', {}))
        self.ormclass_list = []
        self.label_list = []
        self.alias_list = []
        self.alias_dict = {}
        for path_spec in queryhelp.pop('path'):
            self._add_to_path(path_spec)

        self.caller = caller_cls


    def _add_to_path(self, path_spec, autolabel =  False):
        def get_node(path_dict):
            
                ormclass_type = path_dict['class'] 
                
                label = path_dict['label']
                return ormclass,  label
            
        path_spec= make_json_compatible(path_spec)
        
        if isinstance(path_spec, dict):
            assert 'class' in path_spec.keys(), 'You need to provide a key "class" with the value being an ORMClass or a unique type'
        else:
            path_spec = {'class': path_spec}
        ormclass_type =  path_spec['class']
        if autolabel:
            i = 1
            while True:
                label = '{}_{}'.format(ormclass_type, i)
                if label not in self.label_list:
                    break
                i  += 1
        else:
            label = path_spec.get('label',ormclass_type)
            if label in self.label_list:
                raise Exception (' Label {} is not unique, choose different label'.format(label))
        
        
        
        self.label_list.append(label)
        path_spec['label'] = label
        
        try:
            ormclass = ormclass_dict[ormclass_type]
            self.ormclass_list.append(ormclass)
        except KeyError:
            raise Exception (   'Unknown type {}\n'
                                'These are the types I know about:\n'
                                '{}'.format(ormclass_type, ormclass_dict.keys()))
        
        #~ print label
        alias = aliased(ormclass)
        self.alias_list.append(alias)
        self.alias_dict[label] = alias 
        
        
        self.path.append(path_spec)
        
        
        return label 

        

        
    def _get_column(self,colname, alias):
        """
        Return the column for the projection, if the column name is specified.

        TODO:
         
         - Is there a less verbose way to return the column:
         
         - does not work: alias.__table__.columns[colname]
        """
        if colname == 'extras':
            return alias.extras
        if colname == 'attributes':
            return alias.attributes
        elif colname == 'name':
            return alias.name
        elif colname == 'state':
            return alias.state
        elif colname == 'id':
            return alias.id
        elif colname == 'type':
            return alias.type
        elif colname == 'time':
            return alias.time
            
        raise Exception('Unknown colname {}'.format (colname))
        
        
    def _build_query(self, session):
        """
        build the query and return a sqlalchemy.Query instance
        """

        def join_outputs(toconnectwith, alias):
            link = aliased(Link)    
            self.que = self.que.join(link, link.input_id == toconnectwith.id)
            self.que = self.que.join(alias, link.output_id == alias.id)
        
        def join_inputs(toconnectwith, alias):
            link = aliased(Link)  
            self.que = self.que.join(link, link.output_id ==toconnectwith.id)
            self.que = self.que.join(alias, link.input_id == alias.id)
        
        def get_connecting_node(querydict, index):
            input_of = querydict.get('input_of', False)
            output_of = querydict.get('output_of',False)
            assert not(input_of  and output_of), 'You cannot specify both input and output in {}'.format(querydict)
            if input_of:
                func = join_inputs
                val = input_of
                if isinstance(val, int):
                    return aliases[val], func
                elif isinstance(val, str):
                    try:
                        val = labels_location_dict[val]
                        return aliases[val], func
                    except AttributeError:
                        raise Exception (   'List of types is not unique, '
                                            'therefore you cannot specify types to determine node to connect with. '
                                            'Give the position (integer) in the queryhelp')
                    except KeyError:
                        raise Exception (   'Key {} is unknown to the types I know about:\n {}'.format(val, self.types_location_dict.keys()))
                    
                raise Exception('Unrecognized connection specification {}'.format(val))
            if output_of:
                func = join_outputs
                val = output_of
                if isinstance(val, int):
                    return aliases[val], func
                elif isinstance(val, str):
                    try:
                        val = labels_location_dict[val]
                        return aliases[val], func
                    except TypeError:
                        raise Exception (   'List of labels is not unique, '
                                            'therefore you cannot specify types to determine node to connect with. '
                                            'Give the position (integer) in the queryhelp')
                    except KeyError:
                        raise Exception (   'Key {} is unknown to the types I know about:\n {}'.format(val, self.types_location_dict.keys()))
                    
                raise Exception('Unrecognized connection specification {}'.format(val))
            
            direction = querydict.get('direction', 1)
            if direction > 0 :  
                #I am moving down the tree, from a calculation to the output, from data to the calculations 
                toconnectwith =  aliases[index  - direction]   #table I am connecting with. Eg if alias is a table instance then toconnectwith is a calculation it is an output of
                return  toconnectwith, join_outputs
            elif direction < 0:
                # I am moving up the tree, from an output to the calculation, from a calculation to the input
                toconnectwith =  aliases[index + direction]
                return  toconnectwith, join_inputs
               
               
        def analyze_filter_spec(alias, filter_spec):
            expressions = []
            for path_spec, filter_operation_dict in filter_spec.items():
                if path_spec == 'and':
                    for and_filter in filter_operation_dict:
                        expressions.append(analyze_filter_spec(alias, and_filter))
                    expressions.append(and_(*flatten(expressions)))
                
                elif path_spec == 'or':
                    for and_filter in filter_operation_dict:
                        expressions.append(analyze_filter_spec(alias, and_filter))
                    expressions.append(or_(*flatten(expressions)))
                elif path_spec == '~and':
                    for and_filter in filter_operation_dict:
                        expressions.append(analyze_filter_spec(alias, and_filter))
                    expressions.append(not_(and_(*flatten(expressions))))
                
                elif path_spec == '~or':
                    for and_filter in filter_operation_dict:
                        expressions.append(analyze_filter_spec(alias, and_filter))
                    expressions.append(not_(or_(*flatten(expressions))))
                    
                else:
                    column_name = path_spec.split('.')[0] 
                    column =  self._get_column(column_name, alias)
                    json_path = path_spec.split('.')[1:]
                    db_path = column[(json_path)] if json_path else column
                    val_in_json = bool(json_path)
                    [expressions.append(get_expr(operator, value, db_path, val_in_json)) for operator, value in filter_operation_dict.items()]
            return expressions


            

        # First I need to alias everything, because I might be joining the same table multiple times,
        #check also http://docs.sqlalchemy.org/en/rel_1_0/orm/query.html
        
        
        
        #~ ormclass_list, label_list = zip(*[
                #~ get_node(path_spec) for path_spec in self.path])
        ormclass_list = self.ormclass_list
        label_list = self.label_list 
        assert len(label_list) == len(set(label_list)), "Labels are not unique"
        
        aliases = self.alias_list
        
        labels_location_dict = {label:index for index, label in enumerate(label_list)}
        #Starting the query:
        self.que = session.query(aliases[0])
        #Ok, now the big loop
        
        for index,  alias, querydict  in  zip(range(len(aliases)), aliases, self.path):
            #looping through the queryhelp
            if index:
                #There is nothing to join if that is the first table\
                toconnectwith, connection_func = get_connecting_node(querydict, index)
                connection_func(toconnectwith, alias)
                
            # easy filters:
            # Deprecated, might mess up with a lot of joins !!!! just for very easy queries!
            filter_bys = querydict.get('filter_by',[])
            for filter_by in filter_bys:
                self.que =self.que.filter_by(**filter_by)
            
            user_filters = querydict.get('filter', [])
            if not isinstance(user_filters, list):
                user_filters = [user_filters]
            for user_filter in user_filters:
                self._filter(user_filter, alias = alias)
        
        # PROJECTIONS:
        # first clear the entities in the case the first item in the path was not meant to be projected
        self.que._entities = []
        #~ assert self.projection_dict_user, 'You did not specify anything to be projected'
        if not self.projection_dict_user: 
            # I will simply project the last item specified!
            self.projection_dict_user =  {self.label_list[-1]:'*'}

        self.label_to_projected_entity_dict = {}
        position_index = -1

        for label, items_to_project in self.projection_dict_user.items():
            alias = self.alias_dict[label]
            if not hasattr(items_to_project, '__iter__'):
                items_to_project = [items_to_project]
            self.label_to_projected_entity_dict[label] = {}
            for item_to_project in items_to_project:
                position_index += 1
                self.label_to_projected_entity_dict[label][item_to_project]  = position_index
                if item_to_project == '*': # project the entity
                    self.que = self.que.add_entity(alias)
                else:
                    column_name = item_to_project.split('.')[0] 
                    json_path = item_to_project.split('.')[1:]
                    if json_path:
                    #~ if False and json_path[-1].endswith('time'):
                        #~ self.que = self.que.add_columns(self._get_column(column_name, alias)[json_path].cast(TIMESTAMP))
                        #~ self.que = self.que.add_columns(self._get_column(column_name, alias)[json_path].astext)
                    #~ else:
                        self.que = self.que.add_columns(self._get_column(column_name, alias)[json_path].cast(JSONB))  # JSONB is the JOKER that returns the right type
                    else:
                        self.que =  self.que.add_columns(self._get_column(column_name, alias))

                
        
        ####################### FILTERS ####################
        
        for label, filter_specs in self.filters.items():
            try:
                alias = self.alias_dict[label]
            except KeyError:
                raise KeyError(    ' You looked for label {} among the alias list\n'
                                    'The labels I know are:\n{}'.format(label,self.alias_dict.keys()))
            if isinstance(filter_specs, list):
                expressions  = flatten([analyze_filter_spec(alias, filter_spec) for filter_spec in filter_specs])
            else:
                expressions  = flatten(analyze_filter_spec(alias, filter_specs) )
            self.que = self.que.filter(and_(*expressions))
            print label, len(self.que.all())
        return self.que
    
    
    def get_query(self, session):
        return self._build_query(session)
    
    def all(self, session):
        return self._build_query(session).all()
    
    def get_results_dict(self, session):
        all_results = self._build_query(session).all()
        return_list = [
            {
                label:{
                    key:res[position]
                    for key, position in val.items()
                }
                for label, val in self.label_to_projected_entity_dict.items()
            }
            for res in all_results        
        ]
            
        return return_list
                
        #~ print self.que._entities[0].__dict__
    
    def first(self, session):
        return self._build_query(session).first()
    
    def outputs_p(self, user_filter = []):
        label = self._add_to_path({'class':'node', 'output_of':self.path[-1]['label']}, autolabel = True)
        self.filters.update({label:user_filter})
        return self
        
    def inputs_p(self, user_filter = []):
        label = self._add_to_path({'class':'node', 'input_of':self.path[-1]['label']}, autolabel = True)
        self.filters.update({label:user_filter})
        return self
        
    def get_aliases(self):
        return self.alias_list
    
    def get_dict(self):
        """
        Returns the json-compatible list
        """
        return {
            'path':self.path, 
            'filters':self.filters,
            'project':self.projection_dict_user
        }

