import datetime, hashlib, sys, os, copy  #inspect, traceback
from uuid import uuid4
from sqlalchemy import ForeignKey, UniqueConstraint, create_engine, Boolean, text, or_, not_, and_, except_, except_all, func, distinct
from sqlalchemy import Column, Integer, String, DateTime, Float, select, func
from sqlalchemy.orm import relationship, backref, sessionmaker, with_polymorphic, aliased, Query, Session
from sqlalchemy.orm import load_only
from sqlalchemy.orm.exc import MultipleResultsFound, NoResultFound
from sqlalchemy.sql import exists
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.dialects.postgresql import JSON,JSONB, UUID, ARRAY, JSONElement, TIMESTAMP

### CUSTOM
from sqlalchemy.sql.expression import FunctionElement
from sqlalchemy.ext.compiler import compiles


#~ 
#~ class json_array_length(FunctionElement):
    #~ name = 'json_array_len'
#~ 
#~ @compiles(json_array_length)
#~ def compile(element, compiler, **kw):
    #~ return "json_array_length(%s)" % compiler.process(element.clauses)
#~ 

class jsonb_array_length(FunctionElement):
    name = 'jsonb_array_len'

@compiles(jsonb_array_length)
def compile(element, compiler, **kw):
    """
    Compile a function that postgresql has implemented, but SQLAlchemy has not
    """
    return "jsonb_array_length(%s)" % compiler.process(element.clauses)

#~ 
#~ class array_to_jsonb(FunctionElement):
    #~ name = 'array_to_jsonb'
#~ @compiles(array_to_jsonb)
#~ def compile(element, compiler, **kw):
    #~ return "array_to_jsonb(%s)" % compiler.process(element.clauses)

    
### END CUSTOM

Base = declarative_base()

#### userspecs


############################ MAIN MODEL #######################################


class Node(Base):
    """
    Base-deriving class for all calculations, stores in table "calc"
    """
    __tablename__ = "node"
    
    #The columns:
    id = Column(Integer, primary_key=True)
    uuid = Column(UUID(as_uuid=True), default=uuid4)
    name = Column(String)
    label = Column(String)
    time = Column(DateTime(timezone=False), default=datetime.datetime.now)
    type = Column(String)
    state  =  Column(String)
    extras = Column(JSONB)
    attributes = Column(JSONB)
    outputs = relationship("Node", secondary="link",
                           primaryjoin="Node.id==Link.input_id",
                           secondaryjoin="Node.id==Link.output_id",
                           backref=backref("inputs", lazy = 'dynamic'), lazy = 'dynamic') 
    __mapper_args__ = {'polymorphic_on': type, 'polymorphic_identity': 'node'}
    def __init__(self,name = None, label = None, state = 'new', attributes = {}):
        self.name = name
        self.label = label
        self.state = state
        self.attributes = attributes
    def __repr__(self):
        return '{} ({})'.format(self.name, self.id)

    def outputs_p(self):
        """
        Walk along a path using the querytool!
        """
        from querytool import Querytool
        return Querytool({'path':[{'class':self.type}, {'class':'node', 'output_of':self.type}], 'filters':{self.type:[{'id':{'==':self.id}}]}})
    def inputs_p(self):
        """
        Walk along a path using the querytool!
        """
        from querytool import Querytool
        return Querytool({'path':[{'class':self.type}, {'class':'node', 'input_of':self.type}], 'filters':{self.type:[{'id':{'==':self.id}}]}})

class Link(Base):
    __tablename__ = "link"
    #The columns:
    id = Column(Integer, primary_key=True)
    input_id = Column(Integer, ForeignKey("node.id")) #, ondelete = 'cascade'
    output_id = Column(Integer, ForeignKey("node.id")) #, ondelete = 'cascade'

#subtypes


class Data(Node):
    __mapper_args__ = {'polymorphic_identity': 'node.data'}
class Calc(Node):
    __mapper_args__ = {'polymorphic_identity': 'node.calc'}

class Relax(Calc):
    __mapper_args__ = {'polymorphic_identity': 'node.calc.relax'}
class MD(Calc):
    __mapper_args__ = {'polymorphic_identity': 'node.calc.md'}
class Struc(Data):
    __mapper_args__ = {'polymorphic_identity': 'node.data.struc'}
class Param(Data):
    __mapper_args__ = {'polymorphic_identity': 'node.data.param'}

class Traj(Data):
    __mapper_args__ = {'polymorphic_identity': 'node.data.traj'}



### CONNECT
settings =  {'database': 'aiidaqueries'}
command = "postgresql://{}:{}@{}:{}/{}".format(settings.get('user', False) or os.getlogin(),
                        settings.get('password', False) or os.getlogin(), 
                        'localhost',
                        5432,
                        settings['database'])








engine = create_engine(command)
Base.metadata.create_all(engine)
Session = sessionmaker(bind=engine, expire_on_commit=False)
session = Session()


def get_pg_tc(links_table_name,
              links_table_input_field,
              links_table_output_field,
              closure_table_name,
              closure_table_parent_field,
              closure_table_child_field):
    from string import Template

    pg_tc = Template("""

DROP TRIGGER IF EXISTS autoupdate_tc ON $links_table_name;
DROP FUNCTION IF EXISTS update_tc();

CREATE OR REPLACE FUNCTION update_tc()
  RETURNS trigger AS
$$BODY$$
DECLARE
     
    new_id INTEGER;
    old_id INTEGER;
    num_rows INTEGER;
    
BEGIN

  IF tg_op = 'INSERT' THEN

    IF EXISTS (
      SELECT Id FROM $closure_table_name
      WHERE $closure_table_parent_field = new.$links_table_input_field
         AND $closure_table_child_field = new.$links_table_output_field
         AND depth = 0
         )
    THEN
      RETURN null;
    END IF;

    IF new.$links_table_input_field = new.$links_table_output_field
    OR EXISTS (
      SELECT id FROM $closure_table_name 
        WHERE $closure_table_parent_field = new.$links_table_output_field 
        AND $closure_table_child_field = new.$links_table_input_field
        )
    THEN
      RETURN null;
    END IF;

    INSERT INTO $closure_table_name (
         $closure_table_parent_field,
         $closure_table_child_field,
         depth)
      VALUES (
         new.$links_table_input_field,
         new.$links_table_output_field,
         0);
     
    new_id := lastval();

    UPDATE $closure_table_name
      SET entry_edge_id = new_id
        , exit_edge_id = new_id
        , direct_edge_id = new_id 
      WHERE id = new_id;


    INSERT INTO $closure_table_name (
      entry_edge_id,
      direct_edge_id,
      exit_edge_id,
      $closure_table_parent_field,
      $closure_table_child_field,
      depth) 
      SELECT id
         , new_id
         , new_id
         , $closure_table_parent_field 
         , new.$links_table_output_field
         , depth + 1
        FROM $closure_table_name
        WHERE $closure_table_child_field = new.$links_table_input_field;


    INSERT INTO $closure_table_name (
      entry_edge_id,
      direct_edge_id,
      exit_edge_id,
      $closure_table_parent_field,
      $closure_table_child_field,
      depth)
      SELECT new_id
        , new_id
        , id 
        , new.$links_table_input_field
        , $closure_table_child_field
        , depth + 1
        FROM $closure_table_name
        WHERE $closure_table_parent_field = new.$links_table_output_field;

    INSERT INTO $closure_table_name (
      entry_edge_id,
      direct_edge_id,
      exit_edge_id,
      $closure_table_parent_field,
      $closure_table_child_field,
      depth)
      SELECT A.id
        , new_id
        , B.id
        , A.$closure_table_parent_field
        , B.$closure_table_child_field
        , A.depth + B.depth + 2
     FROM $closure_table_name A
        CROSS JOIN $closure_table_name B
     WHERE A.$closure_table_child_field = new.$links_table_input_field
       AND B.$closure_table_parent_field = new.$links_table_output_field;
   
  END IF;

  IF tg_op = 'DELETE' THEN

    IF NOT EXISTS( 
        SELECT id FROM $closure_table_name 
        WHERE $closure_table_parent_field = old.$links_table_input_field
        AND $closure_table_child_field = old.$links_table_output_field AND
        depth = 0 )
    THEN
        RETURN NULL;
    END IF;

    CREATE TABLE PurgeList (Id int);

    INSERT INTO PurgeList
      SELECT id FROM $closure_table_name
          WHERE $closure_table_parent_field = old.$links_table_input_field
        AND $closure_table_child_field = old.$links_table_output_field AND
        depth = 0;
          
    WHILE (1 = 1)
    loop
    
      INSERT INTO PurgeList
        SELECT id FROM $closure_table_name
          WHERE depth > 0
          AND ( entry_edge_id IN ( SELECT Id FROM PurgeList ) 
          OR direct_edge_id IN ( SELECT Id FROM PurgeList ) 
          OR exit_edge_id IN ( SELECT Id FROM PurgeList ) )
          AND Id NOT IN (SELECT Id FROM PurgeList );
          
      GET DIAGNOSTICS num_rows = ROW_COUNT;
      if (num_rows = 0) THEN 
        EXIT;
      END IF;
    end loop;
    
    DELETE FROM $closure_table_name WHERE Id IN ( SELECT Id FROM PurgeList);
    DROP TABLE PurgeList;
    
  END IF;

  RETURN NULL;
  
END
$$BODY$$
  LANGUAGE plpgsql VOLATILE
  COST 100;


CREATE TRIGGER autoupdate_tc
  AFTER INSERT OR DELETE OR UPDATE
  ON $links_table_name FOR each ROW
  EXECUTE PROCEDURE update_tc();

""")

    return pg_tc.substitute(links_table_name=links_table_name,
                            links_table_input_field=links_table_input_field,
                            links_table_output_field=links_table_output_field,
                            closure_table_name=closure_table_name,
                            closure_table_parent_field=closure_table_parent_field,
                            closure_table_child_field=closure_table_child_field)

def install_tc(session):    
    links_table_name = "link"
    links_table_input_field = "input_id"
    links_table_output_field = "output_id"
    closure_table_name = "path"
    closure_table_parent_field = "parent_id"
    closure_table_child_field = "child_id"
    session.execute(get_pg_tc(links_table_name, links_table_input_field, links_table_output_field,
                                 closure_table_name, closure_table_parent_field, closure_table_child_field))

