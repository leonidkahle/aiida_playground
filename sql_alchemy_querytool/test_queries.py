from querytool import Querytool
from new_model import *
import datetime, pytz

my_date = datetime.datetime.now(pytz.timezone('US/Pacific')) 
#~ my_date = datetime.datetime.now() 
print str(my_date)
def write_to_db():
    from random import randint
    
    struc_h2 = Struc ( name = 'H2', attributes  = {'alias':'hydrogen', 'atoms':['H','H'], 'composition':{ 'H':2}, 'positions':[], 'elements':['H'], 'cell': [[12.0, 0,0],[0.0, 12.0,0],[0, 0,12.0]]})
    struc_nh3 = Struc ( name = 'NH3', attributes  = {'atoms':['N','H','H','H'], 'composition':{'N':1, 'H':3}, 'elements':['H','N'],'positions':[], 'cell': [[6, 0,0],[0.0, 6.0,0],[0, 0,6.0]]})
    p_relax = Param(name = 'relax_params', attributes = {'method':'BFGS', 'econv':1e-6, 'sometime':str(my_date)})
    relaxed_h2 = Struc ( name = 'relaxed_H2', attributes  = {'atoms':['H','H'], 'elements':['H'],'composition':{ 'H':2}, 'positions':[], 'cell': [[12.0, 0.,0.],[0.0, 12.0,0.],[0., 0.,12.0]]})
    relaxed_nh3 = Struc ( name = 'relaxed_NH3', attributes  = {'atoms':['N','H','H','H'], 'composition':{'N':1, 'H':3}, 'elements':['H', 'N'], 'positions':[], 'cell': [[6, 0,0],[0.0, 6.0,0],[0, 0,6.0]]})
    p_md1 = Param(state = 'parsing', name = 'md_@_600', attributes = {'SYSTEM':{'econv':1e-6, 'ecut':30}, 'IONS':{'tempw':600, 'method':'BFGS'}})
    p_md2 = Param(state = 'finished', name = 'md_@_100', attributes = {'SYSTEM':{'econv':1e-7, 'ecut':310}, 'IONS':{'tempw':100, 'method':'BFGS'}})
    p_md3 = Param(state = 'finished', name = 'md_@_1000', attributes = {'SYSTEM':{'econv':1e-7, 'ecut':310}, 'IONS':{'tempw':1000, 'method':'BFGS'}})

    # Ok first things let's connect everything
    # the two structures are being relaxed
    relax_h2_calc  = Relax(name = 'relax_h2_calc')
    relax_h2_calc.inputs.append(struc_h2)
    relax_h2_calc.inputs.append(p_relax)
    relax_h2_calc.outputs.append(relaxed_h2)
    session.add(relax_h2_calc)
    
    
    relax_nh3_calc  = Relax(name = 'relax_nh3_calc')
    relax_nh3_calc.inputs.append(struc_nh3)
    relax_nh3_calc.inputs.append(p_relax)
    relax_nh3_calc.outputs.append(relaxed_nh3)
    session.add(relax_nh3_calc)

    for relaxed_struc  in (relaxed_h2, relaxed_nh3):
        for md_param in (p_md1, p_md2, p_md3):
            if relaxed_struc == relaxed_nh3  and md_param == p_md3:
                continue
            md_calc = MD(name = '{}+{}.md'.format(relaxed_struc.name, md_param.name))
            session.add(md_calc)
            md_calc.inputs.append(relaxed_struc)
            md_calc.inputs.append(md_param)
            md_calc.outputs.append(Traj(name = '{}+{}.traj'.format(relaxed_struc.name, md_param.name), attributes = {'path':'...', 'length': randint(1, 10000)}))
    
    
    session.flush()
    #~ session.commit()
    

if __name__ == '__main__':
    write_to_db()

#~ if True:    
    import pprint
    
    
    qh1 = {
        'path': [
            {'class':Struc}, 
            {'class':Relax}
        ],
        'project':{
            Struc:['id', 'name', 'attributes.atoms'], 
            'node.calc.relax':True
        },
        'filters':{
            'node.data.struc':{
                'id':{
                    'or':[
                        {'>':12},
                        {'<':121212}
                    ]
                }
            }
        }
    }

    qh2 = {
        'path':[{'class':MD}, {'class':Traj}, {'class':Param,  'direction':-2}]
    }

    qh3 = {
        'path':[{'class':MD}, {'class':Traj}, {'class':Param,  'input_of':MD}]
    }
    qh4 = {
        'path': [{'class':'node.data.struc'}, {'class':Relax}],
        'filter':{
            'node.data.struc':{
                'id':{'>':50,'<':50000}
            }
        }
    }
    qh5 = {
        'path':[
            {'class':'node.data.struc'},
        ],
        'filters': {
            Struc:{
                'name':{'ilike':'%h%2%'},
            }
        },
        'project':{
            Struc:['id', 'name','attributes.atoms']
        }
    }
    t = datetime.datetime.now(pytz.timezone('US/Pacific')) - datetime.timedelta(days = 1)
    print t
    qh6 = {
        'path':[
            {'class':Relax}, 
            {'class':Struc},
            {'class':Param, 'direction':-2},
        ],
        'project':{
            Relax:'*',
            Struc:'*',
            Param:'attributes'
        },
        'filters':{
            Struc:[
                {
                    'id':{'>': 4},
                }
            ],
            Param:{
                'attributes.sometime':{
                    'or':[
                        {'>':t},
                        {'<':t},
                        {'==':t}
                    ]
                }
            }
            
        }
    }
    qh7 = {
        'path':[
            {'class':'node.data.struc'},
            {'class':Relax},
            {'class':Param,'direction':-1},
            {
                'class':Struc, 
                'direction':2,
                'label':'struc2'
            },
            {'class':Node},
            {   
                'class':Traj,
            }
        ],   
        'project':{
            'node.data.struc':[
                'name', 
                'attributes.composition',
                'attributes.atoms'
            ],
            Relax:'*',
            Param:'attributes.econv',
            Node:['name', 'id'],
            
            
        },
        'filters':{
            'node.data.struc':{
                'and':[
                    {'attributes.cell.0.0':{'>':1.0}}, 
                    {'attributes.atoms':{
                        'contains':['N', 'H'],
                        '~has_key':'S', 
                        '~contains':['S', 'O', 'P'], 
                        'of_length':4
                        }
                    },
                    {'attributes.elements':{'of_length':2}}, 
                ]
            },
            "node":{
                'id':{
                    '>': 5, 
                    '<':50000   
                },
                'type':{'like':'node.calc.%'}
            }
        }
    }
    
    
    qh8 = {
        'path':[
            {'class':Param}, 
            {
                'class':MD,
                'label':'md'
            },
            {'class':Traj}, 
            {
                'class':Struc, 
                'input_of':'md'
            },
            {
                'class':Relax,
                'input_of':Struc
            },
            {
                'class':Struc,
                'label':'struc2',
                'input_of':Relax
            }
        ],
        'project':{
            Param:'attributes.IONS.tempw',
            'md':['id', 'time'], 
            Traj:[
                'id', 
                'attributes.length'
            ],  
            Struc:[
                'id',
                'name', 
                'attributes.atoms',
                'attributes.cell'
            ], 
            'struc2':[
                'id',
                'name', 
                'attributes.atoms',
                'attributes.cell'
            ],
        },
        'filters':{
            Param:{   
                'and':[
                    {'attributes.SYSTEM.econv':{'<':1e-5}},
                    {'attributes.SYSTEM.ecut':{'>':60}}
                ]
                    
            },
            'md':[
                {
                    'state':{   
                        'in':(
                            'computing', 
                            'parsing', 
                            'finished',
                            'new'
                        )
                    }
                }
            ],
            Struc:[
                {
                    'or':[
                        {
                            'attributes.cell.0.0':{
                                'or':[
                                    {'<':3.0},
                                    {'>':5., '<':7.}
                                ]
                            },
                        },
                        {
                            'attributes.cell.1.1':{
                                'or':[
                                    {'<':3.0},
                                    {'>':5., '<':7.}
                                ]
                            },
                        },
                        {
                            'attributes.cell.2.2':{
                                'or':[
                                    {'<':3.0},
                                    {'>':5., '<':7.}
                                ]
                            },
                        },
                    ]
                    
                },
                {
                    'attributes.atoms':{
                        'contains':['N', 'H'],
                        '~has_key':'S'
                    }
                },
                {
                    'attributes.atoms':{
                        'of_length':4
                    }
                },
                {
                    'attributes.elements':{
                        'shorter':3,
                        'has_key':'N',
                    }
                }
            ],
            
        }
    }
    
    
    queries = [qh1, qh2, qh3,qh4,qh5,qh6, qh7, qh8]
    queries = [qh2, qh6]
    if True:
        for queryhelp in queries:
    
            qt = Querytool(queryhelp)
            
            query = qt.get_query(session)
            
            pprint.pprint(qt.get_dict())
            print 
            print query 
            print 
            query = query.all()
            
            if not query:
                print 'EMPTY'
            else:
                print 'RESULTS:'
            for item in query:
                try:
                    print '  ', ' '.join(['{:<25}'.format(i) for i in  item])
                except TypeError:
                    print item
            print '\n'
    
    
    #~ print qh1   
    #~ qt = Querytool(qh1)
    #~ 
    #~ print qt.get_results_dict(session)
    #~ 
    #~ struc_h2 = session.query(Struc).filter(Struc.name == 'H2').one()
    #~ print struc_h2.outputs_p().outputs_p({}).all(session)
    #~ print struc_h2.outputs_p().outputs_p({}).inputs_p().inputs_p().all(session)
    #~ print struc_h2.outputs_p().outputs_p({}).first(session).inputs_p().all(session)
    #~ 
    
    sys.exit()
    
    que =  session.query(Param.name, Param.attributes[('IONS', 'tempw')].cast(Float).label('hellp')).\
                filter( Param.attributes[('IONS', 'tempw')].cast(Float) == func.max(Param.attributes[('IONS', 'tempw')].cast(Float)).select())
    
    # Query the maximum temperature that each structure was computed at:
    
    struc   =  aliased(Struc)
    md      =  aliased(MD)
    param   =  aliased(Param)
    link1   =  aliased(Link)
    link2   =  aliased(Link)
    
    subq = session.query(struc.id,  func.max(param.attributes[('IONS', 'tempw')].cast(Float)).label('maxtemp')).\
                join(link1, link1.input_id == struc.id).\
                join(md, link1.output_id == md.id).\
                join(link2, md.id == link2.output_id).\
                join(param, param.id == link2.input_id).\
                group_by(struc).\
                subquery('subq')

    que = session.query(struc, md, param).\
                join(link1, link1.input_id == struc.id).\
                join(md, link1.output_id == md.id).\
                join(link2, md.id == link2.output_id).\
                join(param, param.id == link2.input_id).\
                join(subq, subq.c.id == struc.id).\
                filter(param.attributes[('IONS', 'tempw')].cast(Float) == subq.c.maxtemp)
            # filter(struc.id == subq.c.id).\
    for item in que.all():
        print item
    print 


    que = session.query(struc.id, func.count(md.id),func.max(param.attributes[('IONS', 'tempw')].cast(Float))).\
                join(link1, link1.input_id == struc.id).\
                join(md, link1.output_id == md.id).\
                join(link2, md.id == link2.output_id).\
                join(param, param.id == link2.input_id).\
                group_by(struc.id)
    print que ,'\n'
    print que ,'\n'
    for item in que.all():
        print item
