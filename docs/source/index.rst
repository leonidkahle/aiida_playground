
Aiida Scrum Meeting
++++++++++++++++++++++


QueryTool
---------

Specifications
================

Background:
    * Transition to SQLAlchemy
    * Possibility to store JSONs in PostgreSQL

To investigate:
    #. Are all filters currently implemented (Django) supportable:
        
        * with SQLAlchemy?
        * using JSONs
    #. Additional possibilities?
    #. Multi-user friendliness?

So far implemented:
    * An arbitrary number of joins
    * Aliasing handled automatically
    * Automatic attachment of filters




Implementation / API
======================

.. note:: Sorry, we have to dig into the code a bit... ``Focus on the code``

 .. toctree::
   :maxdepth: 2

This section describes the querytool class (under development) for querying nodes
with an easy Python interface.

Let's start with the prerequisites:

* A querytool should have the capability to join tables in arbitrary order and to an arbitrary nesting level.
  Constraints should be given by the Database Management System (eg PostgreSQL), not the querytool.

* The node table is accessed multiple times in even moderately complex queries. 
  This means that a table needs to be `aliased`. (See http://docs.sqlalchemy.org/en/rel_1_0/orm/query.html?highlight=aliased#sqlalchemy.orm.aliased).
  If you access the node table several times, you need to make sure that the filters are applied to the correct subclass of node! 
  There is a lot to mess up if the user is not familiar with aliasing.

* The above problems in principle can be solved by launching several queries. 
  This slows the overall query down significantly.
  Ideally, the joining and filtering should be done in one single query, if SQL allows for that.

* A querytool will not be able to solve `every` problem you have. 
  Therefore, it should allow for the experienced user to add the filters he needs.
  The user might want the querytool for the joining, which is a very verbose and cumbersome task,
  and apply his own filters afterwards.
  A querytool should be more of a `querybuilder` than a `querier`

.. automodule:: sql_alchemy_querytool.querytool
   :members:
   :special-members: __init__


Conclusion
===========

The current Querytool supports:
    #. Joins of arbitrary nesting depth fully supported, aliasing automated
    #. Filters on single-value columns work for SQLAlchemy as with Django-backend
    #. Filters inside json compiled and implemented
    #. AND / OR / NAND / NOR clauses, arbitrary nestedness, before or after path-specification

To be done,discussed and/or improved:
    #. Querytool API
    #. Further filtering operations
    #. Performance tests
    



Graph-traversal queries using the Queryhelp
-------------------------------------------

One really nice feature that could be useful in the future is enabling  a graph-traversal language as in 
"real" graph-databases like Neo4j or Titan.
If we have a structure ``struc``, instance of an ORM-class and stored in the database, such a graph-traversal query could be::

    all_children_of_depth_2 = struc.outputs.outputs.all()

This would return all data that was produced from all calculations that had this instance as an input.
In a real-case scenario, we would also apply some filters. 
A use-case could be that we are only interested in MD-calculations and the resulting trajectories that have more than 1000 timesteps::


    resulting_trajectories = struc.\
                        outputs_p({'type':{'like':'%md'}}).\
                        outputs_p([
                            {'attributes.length':{'>':1000}}, 
                            {'type':{'like':'%traj'}}
                        ]).\
                        all()
    
    
Graph-traversal queries are currently not implemented in SQLAlchemy, because the methods/relationships of ORM-instances cannot become the methods of 
a query-instance just like that...
But, using the Querytool, we can append specifications on the fly to the queryhelp, which is a dictionary of lists and dictionaries.

Two such graph-traversal methods are currently implemented:

-   inputs_p
-   outputs_p

..note:: the _p at the end is to distinguish from appender-queries on the defined relationship (``outputs`` and ``inputs``)

Graph-traversal queries can only go along a defined path and project the last given entity.
A current working example is::

    struc_h2.outputs_p().\
            outputs_p({'type':{'like':'%md'}}).\
            inputs_p({'type':{'like':'%param'}}).\
            all(session)

It returns all the parameters that were used for all MD-calculations that this structure was an input for.
Let me know if you see any use-cases...


CP2K-Plugin
-----------



Progress from last meeting:
    * No progress 

To Do:
    * Handling of restarts
    * Linking of basis sets / potentials and calculations 
    * Documentation, documentation, documentation... 

.. ~ 
.. ~ Credits:
.. ~     * Andreas Glöss (UZH)
.. ~     * Tiziano Müller (UZH)
.. ~     * Patrick Seewald (UZH)
.. ~     * Aliaksandr Yakutovich (EMPA)
.. ~ 
.. ~ 

